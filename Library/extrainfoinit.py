from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy, get_debug_queries
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, current_user, auth_token_required, roles_accepted
from datetime import datetime
from flask_security.utils import encrypt_password, login_user, verify_and_update_password
from flask_restful import reqparse, abort, Api, Resource
import json,sys,getopt
# Create app
app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'

app.config['SECURITY_PASSWORD_SALT'] = 'SSSAAALLLTTT'
app.config['SECURITY_TOKEN_AUTHENTICATION_HEADER'] = 'token'
api = Api(app)

# Create database connection object
db = SQLAlchemy(app)


class ExtraLog(db.Model):
    __tablename__ = 'extralog'
    logid = db.Column(db.Integer, primary_key=True, autoincrement=True)
    info = db.Column(db.String(255))
    summary = db.Column(db.String(255))
    detail = db.Column(db.String(255))
    def __init__(self,infotext,summ,dtl):
        self.info = infotext
        self.summary = summ
        self.detail = dtl


@app.before_first_request
def test():
    info=[ExtraLog("一楼北侧24小时自助借还机","一楼北侧24小时自助借还机出现多次断网情况","根据最近几天同学的反馈，该机器时常出现断网的情况。断网的时间主要集中在早上10点左右，但根据维修人员的反馈，并没有观察到断网的情况。请后续的工作人员在对应时间对该机器进行检测。"),
          ExtraLog("一楼北侧24小时自助借还机","一楼北侧24小时自助借还机出现多次断网情况","根据最近几天同学的反馈，该机器时常出现断网的情况。断网的时间主要集中在早上10点左右，但根据维修人员的反馈，并没有观察到断网的情况。"),
          ExtraLog("五楼3D打印机器","3D打印器材不能正常工作","四月七日工作人员获得同学反馈，3D打印器材不能正常工作。该器材处于试运行阶段，需要联系专业设备维修人员进行维修。前台人员需要在该处张贴标识以提示同学。"),
          ExtraLog("五楼3D打印机器","3D打印器材不能正常工作","四月七日工作人员获得同学反馈，3D打印器材不能正常工作。该器材处于试运行阶段，需要联系专业设备维修人员进行维修。故障已修复"),
          ]
    for e in info:
        db.session.add(e)
    db.session.commit()
    
def opthandler(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:",["ifile="])
   except getopt.GetoptError:
      print('Library -i <databaseinfo>)')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('Library -i <databaseinfo>)')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         return arg
if __name__ == '__main__':
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://'+opthandler((sys.argv[1:]))
    app.run(host="0.0.0.0",port=5000,ssl_context='adhoc')