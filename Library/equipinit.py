
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy, get_debug_queries
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, current_user, auth_token_required, roles_accepted
from datetime import datetime
from flask_security.utils import encrypt_password, login_user, verify_and_update_password
from flask_restful import reqparse, abort, Api, Resource
import json,sys,getopt
# Create app
app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'

app.config['SECURITY_PASSWORD_SALT'] = 'SSSAAALLLTTT'
app.config['SECURITY_TOKEN_AUTHENTICATION_HEADER'] = 'token'
api = Api(app)

# Create database connection object
db = SQLAlchemy(app)

# Define models
roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    username = db.Column(db.String(255))
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

class Equipment(db.Model):
    __tablename__ = 'equipment'
    equipment_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    equipmentname = db.Column(db.String(255), unique=True)
    status = db.Column(db.String(255))
    statusdescription = db.Column(db.String(255))
    create_time = db.Column(db.DateTime())
    group = db.Column(db.String(255))
    statusmodifytime = db.Column(db.DateTime())
    equipmenttype = db.Column(db.String(255))
    equipmentarea = db.Column(db.String(255))
    def __init__(self,name,st,ctime,smtime,eqtype,area,stdcpt,grp):
        self.equipmentname = name
        self.status = st
        self.statusdescription = stdcpt
        self.create_time = ctime
        self.statusmodifytime = smtime
        self.equipmenttype = eqtype
        self.equipmentarea = area
        self.group = grp

class EquipmentStatusLog(db.Model):
    __tablename__ = 'equipmentstatuslog'
    equipmentstatuslogid = db.Column(db.Integer, primary_key=True,autoincrement=True)
    equipment_id = db.Column(db.Integer, db.ForeignKey('equipment.equipment_id'))
    statusupdatetime = db.Column(db.DateTime())
    updatefrom = db.Column(db.String(255))
    description = db.Column(db.String(255))
    updateto = db.Column(db.String(255))
    def __init__(self,eqid,updatetime,ufrom,uto,scrp):
        self.equipment_id = eqid
        self.statusupdatetime = updatetime
        self.updatefrom = ufrom
        self.updateto= uto
        self.description = scrp

class EquipmentModifyLog(db.Model):
    __tablename__ = 'equipmentmodifylog'
    eqmodifylogid = db.Column(db.Integer, primary_key=True,autoincrement=True)
    equipment_id = db.Column(db.Integer, db.ForeignKey('equipment.equipment_id'))
    userid = db.Column(db.Integer, db.ForeignKey('user.id'))
    modifytime = db.Column(db.DateTime())
    modifytype = db.Column(db.String(255))
    modifyobject = db.Column(db.String(255))
    modifyfrom = db.Column(db.String(255))
    modifyto = db.Column(db.String(255))
    def __init__(self,eqid,uid,mtime,mtype,mobject,mfrom,mto):
        self.equipment_id = eqid
        self.userid = uid
        self.modifytime = mtime
        self.modifytype = mtype
        self.modifyobject = mobject
        self.modifyfrom = mfrom
        self.modifyto = mto

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)
@app.before_first_request
def test():
    equips=[Equipment('西侧朗读亭','running','2020-12-21 23:59:59','2020-12-21 23:59:59','朗读亭','1楼北侧','-','朗读亭'),
Equipment('东侧朗读亭','running','2020-12-21 23:59:59','2020-12-21 23:59:59','朗读亭','1楼北侧','-','朗读亭'),
Equipment('海恒微图','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','1楼北侧','-','借还机'),
Equipment('瀑布阅读机1-3号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','电子阅读','1楼北侧','-','电子阅读'),
Equipment('新东方在线课堂1楼','running','2020-12-21 23:59:59','2020-12-21 23:59:59','信息服务','1楼北侧','-','信息服务'),
Equipment('24小时自助还书机','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','1楼北侧','-','借还机'),
Equipment('阿法迪微图','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','1楼南侧','-','借还机'),
Equipment('瀑布阅读机4-6号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','电子阅读','1楼南侧','-','电子阅读'),
Equipment('歌德机','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','2楼','-','借还机'),
Equipment('云屏数字借阅机','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','2楼','-','借还机'),
Equipment('自助打印机1号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','自助打印','2楼','-','自助打印'),
Equipment('自助图书杀菌机','running','2020-12-21 23:59:59','2020-12-21 23:59:59','杀菌','2楼','-','杀菌'),
Equipment('自助借还机2楼1号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','2楼','-','借还机'),
Equipment('图书检索机2楼','running','2020-12-21 23:59:59','2020-12-21 23:59:59','信息服务','2楼','-','信息服务'),
Equipment('自助打印机2号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','自助打印','2楼','-','自助打印'),
Equipment('自助借还机2楼2号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','2楼','-','借还机'),
Equipment('自助借还机2楼3号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','2楼','-','借还机'),
Equipment('自助借还机2楼4号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','2楼','-','借还机'),
Equipment('机器人','running','2020-12-21 23:59:59','2020-12-21 23:59:59','机器人','2楼','-','机器人'),
Equipment('选座系统','running','2020-12-21 23:59:59','2020-12-21 23:59:59','座位服务','2楼','-','座位服务'),
Equipment('计算机1—10号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','计算机','2楼','-','计算机'),
Equipment('南侧电梯口显示屏','running','2020-12-21 23:59:59','2020-12-21 23:59:59','媒体播放','2楼','-','媒体播放'),
Equipment('北侧电梯口显示屏','running','2020-12-21 23:59:59','2020-12-21 23:59:59','媒体播放','2楼','-','媒体播放'),
Equipment('选座系统1号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','座位服务','3楼','-','座位服务'),
Equipment('自助借还机3楼1号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','3楼','-','借还机'),
Equipment('自助借还机3楼2号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','3楼','-','借还机'),
Equipment('自助借还机3楼3号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','3楼','-','借还机'),
Equipment('自助借还机3楼4号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','借还机','3楼','-','借还机'),
Equipment('图书检索机3楼','running','2020-12-21 23:59:59','2020-12-21 23:59:59','信息服务','3楼','-','信息服务'),
Equipment('选座系统2号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','座位服务','3楼','-','座位服务'),
Equipment('新东方在线课堂3楼','running','2020-12-21 23:59:59','2020-12-21 23:59:59','信息服务','3楼','-','信息服务'),
Equipment('自助打印机','running','2020-12-21 23:59:59','2020-12-21 23:59:59','自助打印','3楼','-','自助打印'),
Equipment('选座系统3号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','座位服务','4楼','-','座位服务'),
Equipment('mozaik机','running','2020-12-21 23:59:59','2020-12-21 23:59:59','信息服务','5楼','-','信息服务'),
Equipment('北侧展示终端1-10号','running','2020-12-21 23:59:59','2020-12-21 23:59:59','媒体播放','5楼','-','媒体播放'),
Equipment('3D打印','running','2020-12-21 23:59:59','2020-12-21 23:59:59','3D打印','5楼','-','3D打印')]
    for e in equips:
        db.session.add(e)
    db.session.commit()
    

def opthandler(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:",["ifile="])
   except getopt.GetoptError:
      print('Library -i <databaseinfo>)')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('Library -i <databaseinfo>)')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         return arg
if __name__ == '__main__':
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://'+opthandler((sys.argv[1:]))
    app.run(host = "0.0.0.0",port=5000,ssl_context='adhoc')