from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy, get_debug_queries
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, current_user, auth_token_required, roles_accepted
from datetime import datetime
from flask_security.utils import encrypt_password, login_user, verify_and_update_password
from flask_restful import reqparse, abort, Api, Resource
import json,sys,getopt
# Create app
app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'

app.config['SECURITY_PASSWORD_SALT'] = 'SSSAAALLLTTT'
app.config['SECURITY_TOKEN_AUTHENTICATION_HEADER'] = 'token'
api = Api(app)

# Create database connection object
db = SQLAlchemy(app)


class EquipPic(db.Model):
    __tablename__ = 'equippic'
    equipid = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(255))
    def __init__(self,eqid,fn):
        self.equipid = eqid
        self.filename = fn


@app.before_first_request
def test():
    db.create_all()
    db.session.commit()
    
def opthandler(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:",["ifile="])
   except getopt.GetoptError:
      print('Library -i <databaseinfo>)')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('Library -i <databaseinfo>)')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         return arg
if __name__ == '__main__':
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://'+opthandler((sys.argv[1:]))
    app.run(host="0.0.0.0",port=5000,ssl_context='adhoc')