
from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy, get_debug_queries
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, current_user, auth_token_required, roles_accepted
from datetime import datetime
from flask_security.utils import encrypt_password, login_user, verify_and_update_password
from flask_restful import reqparse, abort, Api, Resource
import json,sys,getopt
# Create app
app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'

app.config['SECURITY_PASSWORD_SALT'] = 'SSSAAALLLTTT'
app.config['SECURITY_TOKEN_AUTHENTICATION_HEADER'] = 'token'
api = Api(app)

# Create database connection object
db = SQLAlchemy(app)

# Define models
roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    username = db.Column(db.String(255))
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

class Equipment(db.Model):
    __tablename__ = 'equipment'
    equipment_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    equipmentname = db.Column(db.String(255), unique=True)
    status = db.Column(db.String(255))
    statusdescription = db.Column(db.String(255))
    create_time = db.Column(db.DateTime())
    group = db.Column(db.String(255))
    statusmodifytime = db.Column(db.DateTime())
    equipmenttype = db.Column(db.String(255))
    equipmentarea = db.Column(db.String(255))
    def __init__(self,name,st,ctime,smtime,eqtype,area,stdcpt,grp):
        self.equipmentname = name
        self.status = st
        self.statusdescription = stdcpt
        self.create_time = ctime
        self.statusmodifytime = smtime
        self.equipmenttype = eqtype
        self.equipmentarea = area
        self.group = grp

class EquipmentStatusLog(db.Model):
    __tablename__ = 'equipmentstatuslog'
    equipmentstatuslogid = db.Column(db.Integer, primary_key=True,autoincrement=True)
    equipment_id = db.Column(db.Integer, db.ForeignKey('equipment.equipment_id'))
    statusupdatetime = db.Column(db.DateTime())
    updatefrom = db.Column(db.String(255))
    description = db.Column(db.String(255))
    updateto = db.Column(db.String(255))
    def __init__(self,eqid,updatetime,ufrom,uto,scrp):
        self.equipment_id = eqid
        self.statusupdatetime = updatetime
        self.updatefrom = ufrom
        self.updateto= uto
        self.description = scrp

class EquipmentModifyLog(db.Model):
    __tablename__ = 'equipmentmodifylog'
    eqmodifylogid = db.Column(db.Integer, primary_key=True,autoincrement=True)
    equipment_id = db.Column(db.Integer, db.ForeignKey('equipment.equipment_id'))
    userid = db.Column(db.Integer, db.ForeignKey('user.id'))
    modifytime = db.Column(db.DateTime())
    modifytype = db.Column(db.String(255))
    modifyobject = db.Column(db.String(255))
    modifyfrom = db.Column(db.String(255))
    modifyto = db.Column(db.String(255))
    def __init__(self,eqid,uid,mtime,mtype,mobject,mfrom,mto):
        self.equipment_id = eqid
        self.userid = uid
        self.modifytime = mtime
        self.modifytype = mtype
        self.modifyobject = mobject
        self.modifyfrom = mfrom
        self.modifyto = mto

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)
@app.before_first_request
def test():
    status=[EquipmentStatusLog(3,'2021-04-05 14:23:06','正常','异常','出现故障'),EquipmentStatusLog(3,'2021-04-05 18:35:15','异常','正常','故障修复'),
            EquipmentStatusLog(16,'2021-04-06 09:45:13','正常','异常','出现故障'),EquipmentStatusLog(16,'2021-04-06 14:05:06','异常','正常','故障修复'),
            EquipmentStatusLog(9,'2021-04-06 15:13:51','正常','异常','出现故障'),EquipmentStatusLog(9,'2021-04-06 17:12:26','异常','正常','故障修复')]
    for e in status:
        db.session.add(e)
    db.session.commit()
    

def opthandler(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:",["ifile="])
   except getopt.GetoptError:
      print('Library -i <databaseinfo>)')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('Library -i <databaseinfo>)')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         return arg
if __name__ == '__main__':
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://'+opthandler((sys.argv[1:]))
    app.run(host = "0.0.0.0",port=5000,ssl_context='adhoc')
