from flask import Flask, render_template, send_file
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, current_user, auth_token_required, roles_accepted 

from flask_security.utils import encrypt_password, logout_user, login_user, verify_and_update_password
from flask_restful import reqparse, abort, Api, Resource
import json,sys,getopt,werkzeug,time
# Create app
app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'super-secret'

app.config['SECURITY_PASSWORD_SALT'] = 'SSSAAALLLTTT'
app.config['SECURITY_TOKEN_AUTHENTICATION_HEADER'] = 'token'
api = Api(app)

# Create database connection object
db = SQLAlchemy(app)

# Define models
roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    username = db.Column(db.String(255))
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

class Equipment(db.Model):
    __tablename__ = 'equipment'
    equipment_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    equipmentname = db.Column(db.String(255), unique=True)
    status = db.Column(db.String(255))
    statusdescription = db.Column(db.String(255))
    create_time = db.Column(db.DateTime())
    group = db.Column(db.String(255))
    statusmodifytime = db.Column(db.DateTime())
    equipmenttype = db.Column(db.String(255))
    equipmentarea = db.Column(db.String(255))
    def __init__(self,name,st,ctime,smtime,eqtype,area,stdcpt,grp):
        self.equipmentname = name
        self.status = st
        self.statusdescription = stdcpt
        self.create_time = ctime
        self.statusmodifytime = smtime
        self.equipmenttype = eqtype
        self.equipmentarea = area
        self.group = grp
class ExtraFile(db.Model):
    __tablename__ = 'extrafile'
    fileid = db.Column(db.Integer, primary_key=True, autoincrement=True)
    filename = db.Column(db.String(255), unique=True)
    location = db.Column(db.String(255))
    def __init__(self,name,locat):
        self.filename = name
        self.location = locat
class ExtraLog(db.Model):
    __tablename__ = 'extralog'
    logid = db.Column(db.Integer, primary_key=True, autoincrement=True)
    info = db.Column(db.String(255))
    summary = db.Column(db.String(255))
    detail = db.Column(db.String(255))
    def __init__(self,infotext,summ,dtl):
        self.info = infotext
        self.summary = summ
        self.detail = dtl
class EquipmentStatusLog(db.Model):
    __tablename__ = 'equipmentstatuslog'
    equipmentstatuslogid = db.Column(db.Integer, primary_key=True,autoincrement=True)
    equipment_id = db.Column(db.Integer, db.ForeignKey('equipment.equipment_id'))
    statusupdatetime = db.Column(db.DateTime())
    updatefrom = db.Column(db.String(255))
    description = db.Column(db.String(255))
    updateto = db.Column(db.String(255))
    def __init__(self,eqid,updatetime,ufrom,uto,scrp):
        self.equipment_id = eqid
        self.statusupdatetime = updatetime
        self.updatefrom = ufrom
        self.updateto= uto
        self.description = scrp
class EquipPic(db.Model):
    __tablename__ = 'equippic'
    equipid = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(255))
    def __init__(self,eqid,fn):
        self.equipid = eqid
        self.filename = fn
class EquipmentModifyLog(db.Model):
    __tablename__ = 'equipmentmodifylog'
    eqmodifylogid = db.Column(db.Integer, primary_key=True,autoincrement=True)
    equipment_id = db.Column(db.Integer, db.ForeignKey('equipment.equipment_id'))
    userid = db.Column(db.Integer, db.ForeignKey('user.id'))
    modifytime = db.Column(db.DateTime())
    modifytype = db.Column(db.String(255))
    modifyobject = db.Column(db.String(255))
    modifyfrom = db.Column(db.String(255))
    modifyto = db.Column(db.String(255))
    def __init__(self,eqid,uid,mtime,mtype,mobject,mfrom,mto):
        self.equipment_id = eqid
        self.userid = uid
        self.modifytime = mtime
        self.modifytype = mtype
        self.modifyobject = mobject
        self.modifyfrom = mfrom
        self.modifyto = mto

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


registerParser = reqparse.RequestParser()
registerParser.add_argument('username', type=str, required=True, location='args')
registerParser.add_argument('email', type=str, required=True, location='args')
registerParser.add_argument('password', type=str, required=True, location='args')
registerParser.add_argument('referer', type=str, required=True, location='args')



class Register(Resource):
    def post(self):
        args = registerParser.parse_args()
        if(self.__refererjudge(args['referer'])):
            if(self.__dupjudge(args['email'])):
                newUser = user_datastore.create_user(username=args['username'], email=args['email'], password=encrypt_password(args['password']))
                user_datastore.add_role_to_user(newUser, user_datastore.find_role('user'))
                db.session.commit()
                login_user(newUser)
                return {'status': 'success', 'message': 'register succeed', 'token': newUser.get_auth_token()}
            else:
                return {'status': 'fail', 'message': 'duplicate user'}
        else:
            return {'status': 'fail', 'message': 'unauthorized register'}
    def __refererjudge(self, referer):
        #judge referer, to be added
        return True;
    def __dupjudge(self, email):
        if db.session.query(User).filter(User.email==email).first() != None:
            return False;
        else:
            return True;

loginParser = reqparse.RequestParser()
loginParser.add_argument('email', type=str, required=True)
loginParser.add_argument('password', type=str, required=True)
loginParser.add_argument('referer', type=str, required=True)
class Login(Resource):
    def post(self):
        args = loginParser.parse_args()
        if self.__refererjudge(args['referer']):
            user = user_datastore.find_user(email = args['email'])
            if user != None:
                if verify_and_update_password(args['password'], user):
                    login_user(user)
                    return {'status': 'success', 'message': 'login succeed', 'token': user.get_auth_token()}
                else:
                    return {'status': 'failed', 'message': 'wrong password'}
            else:
                return {'status': 'fail', 'message': 'user not exist'}
        else:
            return {'status': 'fail', 'message': 'unauthorized login'}

    def __refererjudge(self, referer):
        #judge referer, to be added
        return True;

api.add_resource(Register, '/library/api/user/register')
api.add_resource(Login, '/library/api/user/login')

class Relogin(Resource):
    @roles_accepted('admin', 'user')
    def get(self):
        return { 'token': current_user.get_auth_token()}
api.add_resource(Relogin,'/library/api/user/relogin')

class UserTest(Resource):
    @roles_accepted('admin', 'user')
    #@auth_token_required
    def get(self):
        return {'status': "success"}
api.add_resource(UserTest, '/library/api/test/usertest')

class AdminTest(Resource):
    #@auth_token_required
    @roles_accepted('admin')
    def get(self):
        return {'status': "success"}
api.add_resource(AdminTest, '/library/api/test/admintest')


getEquipParser = reqparse.RequestParser()
getEquipParser.add_argument('name', type=str, location='args')
getEquipParser.add_argument('area', type=str, location='args')
getEquipParser.add_argument('type', type=str, location='args')
getEquipParser.add_argument('status', type=str, location='args')
getEquipParser.add_argument('id', type=int, location='args')
getEquipParser.add_argument('number', type=int, location='args')
getEquipParser.add_argument('statusnot', type=str, location='args')
getEquipParser.add_argument('group', type=str, location='args')
def selectEquip(name = None, area= None, type = None, id = None, status = None, number = None,statusnot = None,group = None):
    result = []
    if name == None:
        q = db.session.query(Equipment.equipment_id, Equipment.equipmentname, Equipment.status, Equipment.create_time, Equipment.equipmentarea, Equipment.equipmenttype, Equipment.statusmodifytime,Equipment.group,Equipment.statusdescription)
    else:
        q = db.session.query(Equipment.equipment_id, Equipment.equipmentname, Equipment.status, Equipment.create_time, Equipment.equipmentarea, Equipment.equipmenttype, Equipment.statusmodifytime,Equipment.group,Equipment.statusdescription).filter(name == Equipment.equipmentname)
    if area != None:
        q = q.filter(area == Equipment.equipmentarea)
    if type != None:
        q = q.filter(type == Equipment.equipmenttype)
    if id != None:
        q = q.filter(id == Equipment.equipment_id)
    if status != None:
        q = q.filter(status == Equipment.status)
    if group != None:
        q = q.filter(group == Equipment.status)
    if statusnot != None:
        q = q.filter(statusnot != Equipment.status)
    for i in q.all():
        temp = {}
        temp['equipid']=i[0]
        temp['name']=i[1]
        temp['status']=i[2]
        temp['createtime']=i[3].isoformat()
        temp['area']=i[4]
        temp['type']=i[5]
        temp['statusupdatetime']=i[6].isoformat()
        temp['group']=i[7]
        temp['statusdescription']=i[8]
        result.append(temp)
    if number != None:
        return result[0:number]
    else:
        return result

class GetEquipNumber(Resource):
    @roles_accepted('admin', 'user')
    
    def get(self):
        args = getEquipParser.parse_args()
        if args['name'] == None:
            q = db.session.query(Equipment.equipment_id, Equipment.equipmentname, Equipment.status, Equipment.create_time, Equipment.equipmentarea, Equipment.equipmenttype, Equipment.statusmodifytime,Equipment.group,Equipment.statusdescription)
        else:
            q = db.session.query(Equipment.equipment_id, Equipment.equipmentname, Equipment.status, Equipment.create_time, Equipment.equipmentarea, Equipment.equipmenttype, Equipment.statusmodifytime,Equipment.group,Equipment.statusdescription).filter(args['name'] == Equipment.equipmentname)
        if args['area'] != None:
            q = q.filter(args['area'] == Equipment.equipmentarea)
        if args['type'] != None:
            q = q.filter(args['type'] == Equipment.equipmenttype)
        if args['id'] != None:
            q = q.filter(args['id'] == Equipment.equipment_id)
        if args['status'] != None:
            q = q.filter(args['status'] == Equipment.status)
        if args['statusnot'] != None:
            q = q.filter(args['statusnot'] != Equipment.status)
        if args['group'] != None:
            q = q.filter(args['group'] == Equipment.status)
        return {'number' : q.count()}
api.add_resource(GetEquipNumber, '/library/api/query/equipnumber')

class GetInfermation(Resource):
    @roles_accepted('admin', 'user')
    def get(self):
        args = getEquipParser.parse_args()
        return selectEquip(name = args['name'], area = args['area'], type = args['type'], id = args['id'], status = args['status'], statusnot = args['statusnot'], number = args['number'])
api.add_resource(GetInfermation, '/library/api/query/equipinf')   

getStatusParser = reqparse.RequestParser()
getStatusParser.add_argument('id', type=int, required = True, location='args')
class GetStatus(Resource):
    @roles_accepted('admin', 'user')
    def get(self):
        args = getStatusParser.parse_args()
        result = {}
        temp = selectEquip(id = args['id'])
        if len(temp) == 0:
            result['requeststatus'] = 'failed'
            result['message'] = 'id not exist'
            return result
        else:
            result['requeststatus'] = 'succeed'
            result['message'] = 'request succeed'
            result['equipstatus'] = temp[0]['status']
            result['statusupdatetime'] = temp[0]['statusupdatetime']
            result['statusdescription'] = temp[0]['statusdescription']
            return result
api.add_resource(GetStatus, '/library/api/query/equipstatus')

class GetStatusLog(Resource):
    @roles_accepted('admin',' user')
    def get(self):
        args = getStatusParser.parse_args()
        result = []
        q = db.session.query(EquipmentStatusLog.equipmentstatuslogid, EquipmentStatusLog.statusupdatetime, EquipmentStatusLog.updatefrom, EquipmentStatusLog.updateto,EquipmentStatusLog.description).filter(args['id'] == EquipmentStatusLog.equipment_id).all()
        for i in q:
            temp = {}
            temp['logid']=i[0]
            temp['updatetime']=i[1].isoformat()
            temp['updatefrom']=i[2]
            temp['updateto']=i[3]
            temp['description']=i[4]
            result.append(temp)
        return result
api.add_resource(GetStatusLog, '/library/api/query/equipstatuslog')
getRecentStatusParser = reqparse.RequestParser()
getRecentStatusParser.add_argument('num', type=int, default = 5, location='args')
class GetRecentStatusLog(Resource):
    @roles_accepted('admin',' user')
    def get(self):
        args = getRecentStatusParser.parse_args()
        result = []
        q = db.session.query(EquipmentStatusLog.equipmentstatuslogid,EquipmentStatusLog.equipment_id, EquipmentStatusLog.statusupdatetime, EquipmentStatusLog.updatefrom, EquipmentStatusLog.updateto,EquipmentStatusLog.description).order_by(EquipmentStatusLog.equipmentstatuslogid.desc()).limit(args['num'])
        for i in q:
            temp = {}
            temp['logid']=i[0]
            temp['equipid']=i[1]
            temp['updatetime']=i[2].isoformat()
            temp['updatefrom']=i[3]
            temp['updateto']=i[4]
            temp['description']=i[5]
            result.append(temp)
        return result
api.add_resource(GetRecentStatusLog, '/library/api/query/recentstatuslog')
getExtraLogParser = reqparse.RequestParser()
getExtraLogParser.add_argument('logid', type=int, required = True, location='args')
class GetExtraLog(Resource):
    @roles_accepted('admin',' user')
    def get(self):
        args = getExtraLogParser.parse_args()
        q = db.session.query(ExtraLog.logid,ExtraLog.info,ExtraLog.summary,ExtraLog.detail).filter(ExtraLog.logid == args['logid']).all()
        result = {}
        if len(q) == 0:
            result['requeststatus'] = 'failed'
            result['message'] = 'log not exist'
            return result
        for i in q:
            result['logid']=i[0]
            result['brief_info']=i[1]
            result['summary']=i[2]
            result['detail_text']=i[3]
        return result
api.add_resource(GetExtraLog, '/library/api/query/logdetail')

setStatusParser = reqparse.RequestParser()
setStatusParser.add_argument('id', type=int, required = True, location='args')
setStatusParser.add_argument('updateto', type=str, required = True, location='args')
setStatusParser.add_argument('description', type=str, default = '', location='args')
class setStatus(Resource):
    @roles_accepted('admin')
    def post(self):
        args = setStatusParser.parse_args()
        result = {}
        q=db.session.query(Equipment).filter(Equipment.equipment_id == args['id']).all()
        if len(q) == 0:
            result['status']='failed'
            resule['message']='equipment not exist'
            return result
        slog=EquipmentStatusLog(args['id'], time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) ,q[0].status,args['updateto'],args['description'])
        s=q[0]
        s.status=args['updateto']
        s.statusdescription = args['description']
        s.statusmodifytime = slog.statusupdatetime
        db.session.add(slog)
        db.session.commit()
        result['status']='succeed'
        return result
api.add_resource(setStatus, '/library/api/set/status')

class GetEquipModifyLog(Resource):
    @roles_accepted('admin',' user')
    def get(self):
        args = getStatusParser.parse_args()
        result = []
        q = db.session.query(EquipmentModifyLog.eqmodifylogid, EquipmentModifyLog.modifyobject, EquipmentModifyLog.modifytime, EquipmentModifyLog.modifytype, EquipmentModifyLog.userid,EquipmentModifyLog.modifyfrom,EquipmentModifyLog.modifyto).filter(args['id'] == EquipmentStatusLog.equipment_id).all()
        for i in q:
            temp = {}
            temp['logid']=i[0]
            temp['modifytime']=i[2].isoformat()
            temp['modifyobject']=i[1]
            temp['modifytype']=i[3]
            temp['userid'] = i[4]
            temp['modifyfrom'] = i[5]
            temp['modifyto'] = i[6]
            result.append(temp)
        return result
api.add_resource(GetEquipModifyLog, '/library/api/query/equipmodifylog')
getFileParser = reqparse.RequestParser()
getFileParser.add_argument('filename', type=str, required = True, location='args')

class GetFile(Resource):
    def get(self):
        args = getFileParser.parse_args()
        q = db.session.query(ExtraFile.location,ExtraFile.filename).filter(args['filename'] == ExtraFile.filename).all()
        
        if len(q) == 0:
            result = {}
            result['requeststatus'] = 'failed'
            result['message'] = 'file not exist'
            return result
        else:
            return send_file(q[0][0])
api.add_resource(GetFile, '/library/api/query/file')


postFileParser = reqparse.RequestParser()
postFileParser.add_argument('filename', type=str, required = True, location='args')
postFileParser.add_argument('file', type=werkzeug.datastructures.FileStorage, required = True, location='files')
class PostFile(Resource):
    @roles_accepted('admin')
    def post(self):
        args = postFileParser.parse_args()
        q = db.session.query(ExtraFile.location,ExtraFile.filename).filter(args['filename'] == ExtraFile.filename).all()
        if len(q) != 0:
            result = {}
            result['requeststatus'] = 'failed'
            result['message'] = 'duplicated filename'
            return result
        else:
            uploaded_file = args['file']
            uploaded_file.save('files/'+args['filename'])
            newfile = ExtraFile(args['filename'],'files/'+args['filename'])
            db.session.add(newfile)
            db.session.commit()
            result = {}
            result['requeststatus'] = 'succeed'
            return result
api.add_resource(PostFile, '/library/api/upload/file')

postPicParser = reqparse.RequestParser()
postPicParser.add_argument('equipid', type=int, required = True, location='args')
postPicParser.add_argument('file', type=werkzeug.datastructures.FileStorage, required = True, location='files')
class PostEquipPic(Resource):
    @roles_accepted('admin')
    def post(self):
        
        args = postPicParser.parse_args()
        uploaded_file = args['file']
        filename = 'files/'+str(args['equipid'])+'-'+time.strftime("%Y%m%d%H%M%S", time.localtime())+".jpg"
        uploaded_file.save(filename)
        q = db.session.query(EquipPic).filter(args['equipid'] == EquipPic.equipid).all()
        if len(q) != 0:
            file=q[0]
            file.filename = filename
        else:
            newfile = EquipPic(args['equipid'],filename)
            db.session.add(newfile)
        db.session.commit()
        result = {}
        result['requeststatus'] = 'succeed'
        return result
api.add_resource(PostEquipPic, '/library/api/upload/equippic')
getPicParser = reqparse.RequestParser()
getPicParser.add_argument('equipid', type=int, required = True, location='args')
class GetEquipPic(Resource):
    def get(self):
        args = getPicParser.parse_args()
        q = db.session.query(EquipPic.equipid,EquipPic.filename).filter(args['equipid'] == EquipPic.equipid).all()
        
        if len(q) == 0:
            result = {}
            result['requeststatus'] = 'failed'
            result['message'] = 'file not exist'
            return result
        else:
            return send_file(q[0][1])
api.add_resource(GetEquipPic, '/library/api/query/equippic')

def opthandler(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:",["ifile="])
   except getopt.GetoptError:
      print('Library -i <databaseinfo>)')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('Library -i <databaseinfo>)')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         return arg

if __name__ == '__main__':
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://'+opthandler((sys.argv[1:]))
    app.run(host = "0.0.0.0",port=5000,ssl_context='adhoc')
